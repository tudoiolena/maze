# React + TypeScript + Vite

This app presents a simple maze game with hidden walls for 2 players. The one who reaches the exit first wins! There's also chat implementation.

## Running the app

```bash
# development
$ npm run dev


```
