import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import { SignIn } from "./components/sign-in";
import { Paths } from "./common/enums/paths.enum";
import { Game } from "./components/game";

const App = () => (
  <BrowserRouter>
    <Routes>
      <Route path={Paths.DASHBOARD} element={<SignIn />} />
      <Route path={Paths.GAME} element={<Game />} />
    </Routes>
  </BrowserRouter>
);

export default App;
