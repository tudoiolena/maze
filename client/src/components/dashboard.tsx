import { FC, useContext, useEffect, useState } from "react";
import { Button } from "@mui/material";
import { Paths } from "../common/enums/paths.enum";
import { useNavigate } from "react-router-dom";
import { SocketContext } from "../context/socket";

interface IProps {
  name: string;
}

interface IGame {
  id: string;
  initiatedAt: Date;
  playerName: string;
}

export const Dashboard: FC<IProps> = ({ name }) => {
  const [message, setMessage] = useState("");
  const [isSecondPlayer, setIsSecondPlayer] = useState(false);
  const [gameData, setGameData] = useState<IGame | null>(null);
  const [currentTime, setCurrentTime] = useState(Date.now());
  const socket = useContext(SocketContext);
  const navigate = useNavigate();

  const handleNewGame = () => {
    socket.emit("createGame", { playerName: name });
    socket.emit("generateMaze", { rows: 20, cols: 20 });
  };

  const handleJoinGame = () => {
    navigate(Paths.GAME);
    socket.emit("joinGame", { playerName: name });
  };

  useEffect(() => {
    socket.on("gameCreated", (data) => {
      setGameData(data);
      setMessage(data.waitingMessage);
      setIsSecondPlayer(data.playerName !== name);
    });

    const intervalId = setInterval(() => {
      setCurrentTime(Date.now());
    }, 1000);

    return () => {
      clearInterval(intervalId);
    };
  }, [socket, name]);

  useEffect(() => {
    if (gameData) {
      const elapsedTime =
        currentTime - new Date(gameData.initiatedAt).getTime();
      const minutes = Math.floor(elapsedTime / (1000 * 60));
      const seconds = Math.floor((elapsedTime / 1000) % 60);
      let waitingMessage;

      if (gameData.playerName === name) {
        waitingMessage = `You started a new game ${minutes} minutes ${seconds} seconds ago. Waiting for a second player...`;
      } else {
        waitingMessage = `${gameData.playerName} started a new game ${minutes} minutes ${seconds} seconds ago. Join and be a second player!`;
      }

      setMessage(waitingMessage);
    }
  }, [currentTime, gameData, name]);

  useEffect(() => {
    socket.on("navigateToGame", () => {
      navigate(Paths.GAME);
    });

    return () => {
      socket.off("navigateToGame");
    };
  }, [navigate, socket]);

  return (
    <div>
      <h2>Hello, {name}!</h2>
      {isSecondPlayer ? (
        <Button
          color="secondary"
          size="large"
          variant="contained"
          onClick={handleJoinGame}
        >
          Join the game
        </Button>
      ) : (
        <Button
          color="secondary"
          size="large"
          variant="contained"
          onClick={handleNewGame}
        >
          New Game
        </Button>
      )}

      {message && <p>{message}</p>}
    </div>
  );
};
