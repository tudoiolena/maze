import { useState, useEffect, FC, useContext } from "react";
import { SocketContext } from "../context/socket";
import { ARROW_KEYS, DIRECIONS } from "../common/constants/movement.constants";

interface IProps {
  rows: number;
  cols: number;
  handleWinnerStatus: (winner: boolean) => void;
}

interface IPosition {
  row: number;
  col: number;
}

export const Maze: FC<IProps> = ({ rows, cols, handleWinnerStatus }) => {
  const [maze, setMaze] = useState<number[][]>([]);
  const [player1Position, setPlayer1Position] = useState<IPosition>({
    row: 0,
    col: 0,
  });
  const [player2Position, setPlayer2Position] = useState<IPosition>({
    row: 0,
    col: 0,
  });
  const [exitPosition, setExitPosition] = useState<IPosition>({
    row: 0,
    col: 0,
  });
  const [playerBreadcrumbs, setPlayerBreadcrumbs] = useState<IPosition[]>([]);
  const [hitWalls, setHitWalls] = useState<{ [key: string]: boolean }>({});
  const [isPlayer1Turn, setIsPlayer1Turn] = useState<boolean>(true);
  const [turnMessage, setTurnMessage] = useState<string>("");
  const [winner, setWinner] = useState<string>("");

  const socket = useContext(SocketContext);

  const handleArrowButtonClick = (direction: string) => {
    const key =
      ARROW_KEYS[`ARROW_${direction.toUpperCase()}` as keyof typeof ARROW_KEYS];
    handlePlayerMove(isPlayer1Turn ? player1Position : player2Position, key);
  };

  const handlePlayerMove = (currentPosition: IPosition, key: string) => {
    const { row, col } = currentPosition;
    switch (key) {
      case ARROW_KEYS.ARROW_UP:
        if (row > 0) {
          socket.emit("move", DIRECIONS.UP);
        }
        break;
      case ARROW_KEYS.ARROW_DOWN:
        if (row < rows - 1) {
          socket.emit("move", DIRECIONS.DOWN);
        }
        break;
      case ARROW_KEYS.ARROW_LEFT:
        if (col > 0) {
          socket.emit("move", DIRECIONS.LEFT);
        }
        break;
      case ARROW_KEYS.ARROW_RIGHT:
        if (col < cols - 1) {
          socket.emit("move", DIRECIONS.RIGHT);
        }
        break;
      default:
        break;
    }
  };

  useEffect(() => {
    socket.emit("generateMaze", { rows, cols });
    socket.on(
      "mazeState",
      (data: {
        maze: number[][];
        exitPosition: IPosition;
        initialPlayer1Position: IPosition;
        initialPlayer2Position: IPosition;
      }) => {
        setMaze(data.maze);
        setExitPosition(data.exitPosition);
        setPlayer1Position(data.initialPlayer1Position);
        setPlayer2Position(data.initialPlayer2Position);
      }
    );
    if (winner !== "") {
      socket.emit("playerWins", winner);
    }
    return () => {
      socket.off("mazeState");
    };
  }, [socket, rows, cols, winner]);

  useEffect(() => {
    const handlePlayerMove = (currentPosition: IPosition, key: string) => {
      const { row, col } = currentPosition;
      switch (key) {
        case ARROW_KEYS.ARROW_UP:
          if (row > 0) {
            socket.emit("move", DIRECIONS.UP);
          }
          break;
        case ARROW_KEYS.ARROW_DOWN:
          if (row < rows - 1) {
            socket.emit("move", DIRECIONS.DOWN);
          }
          break;
        case ARROW_KEYS.ARROW_LEFT:
          if (col > 0) {
            socket.emit("move", DIRECIONS.LEFT);
          }
          break;
        case ARROW_KEYS.ARROW_RIGHT:
          if (col < cols - 1) {
            socket.emit("move", DIRECIONS.RIGHT);
          }
          break;
        default:
          break;
      }
    };

    const handleKeyPress = (event: KeyboardEvent) => {
      const { key } = event;
      handlePlayerMove(isPlayer1Turn ? player1Position : player2Position, key);
    };

    window.addEventListener("keydown", handleKeyPress);
    return () => {
      window.removeEventListener("keydown", handleKeyPress);
    };
  }, [
    player1Position,
    player2Position,
    maze,
    playerBreadcrumbs,
    hitWalls,
    exitPosition,
    rows,
    cols,
    socket,
    isPlayer1Turn,
    handleWinnerStatus,
    winner,
  ]);

  useEffect(() => {
    socket.on("playerMoved", (data) => {
      const { player, newPosition } = data;
      if (player === "player1") {
        setPlayer1Position(newPosition);
      } else if (player === "player2") {
        setPlayer2Position(newPosition);
      }

      const { row, col } = newPosition;

      setPlayerBreadcrumbs((prevPlayerTrail) => [
        ...prevPlayerTrail,
        { row, col, player },
      ]);
    });

    return () => {
      socket.off("playerMoved");
    };
  }, [socket]);

  useEffect(() => {
    socket.on("turnMessage", (message: string) => {
      setTurnMessage(message);
    });

    return () => {
      socket.off("turnMessage");
    };
  }, [socket]);

  useEffect(() => {
    socket.on("wallHit", (data) => {
      const { newPosition } = data;
      const { row, col } = newPosition;

      setHitWalls((prevHitWalls) => ({
        ...prevHitWalls,
        [`${row}-${col}`]: true,
      }));
    });

    return () => {
      socket.off("wallHit");
    };
  }, [socket]);

  return (
    <div>
      <h2>Maze Generator</h2>
      <h3 className="turn-message">{turnMessage}</h3>
      <div className="arrow-buttons">
        <button
          className="arrow"
          onClick={() => handleArrowButtonClick(DIRECIONS.UP)}
        >
          ↑
        </button>
        <div>
          <button
            className="arrow"
            onClick={() => handleArrowButtonClick(DIRECIONS.LEFT)}
          >
            ←
          </button>
          <button
            className="arrow"
            onClick={() => handleArrowButtonClick(DIRECIONS.DOWN)}
          >
            ↓
          </button>
          <button
            className="arrow"
            onClick={() => handleArrowButtonClick(DIRECIONS.RIGHT)}
          >
            →
          </button>
        </div>
      </div>
      <div>
        {maze.map((row, rowIndex) => (
          <div key={rowIndex} className="maze-container">
            {row.map((cell, colIndex) => (
              <div
                className={`maze-cell ${
                  hitWalls[`${rowIndex}-${colIndex}`] ? "wall" : "empty"
                }`}
                key={`${rowIndex}-${colIndex}`}
              >
                {player1Position.row === rowIndex &&
                  player1Position.col === colIndex &&
                  "😀"}
                {player2Position.row === rowIndex &&
                  player2Position.col === colIndex &&
                  "🤖"}
                {exitPosition.row === rowIndex &&
                  exitPosition.col === colIndex &&
                  "🏆"}
                {playerBreadcrumbs.some(
                  ({ row, col }) => row === rowIndex && col === colIndex
                ) && <span className="breadcrumbs">•</span>}
              </div>
            ))}
          </div>
        ))}
      </div>
    </div>
  );
};
