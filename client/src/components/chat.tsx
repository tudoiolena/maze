import { FC, useContext, useEffect, useState } from "react";
import { SocketContext } from "../context/socket";
import { Button } from "@mui/material";
import SendIcon from "@mui/icons-material/Send";

interface IProps {
  currentUser: string;
}

type MessageType = {
  text: string;
  sender: string;
  timestamp: number;
  type: string;
};

export const Chat: FC<IProps> = ({ currentUser }) => {
  const [messages, setMessages] = useState<MessageType[]>([]);
  const [newMessage, setNewMessage] = useState("");
  const socket = useContext(SocketContext);

  const getMessageClassName = (type: string) => {
    switch (type) {
      case "playerMove":
        return "player-move-message";
      case "playerHitWall":
        return "hit-wall-message";
      case "giveUp":
        return "give-up-message";
      case "playerWins":
        return "player-wins-message";
      default:
        return "";
    }
  };

  useEffect(() => {
    const handleNewMessage = (message: MessageType) => {
      const timestamp = new Date().toLocaleTimeString();
      message.text = `${timestamp} <${message.sender}>: ${message.text}`;
      setMessages([...messages, message]);
    };

    const handleGiveUpMessage = (giveUpMessage: MessageType) => {
      setMessages([...messages, giveUpMessage]);
    };

    const handlePlayerMoveMessage = (playerMoveMessage: MessageType) => {
      setMessages([...messages, playerMoveMessage]);
    };

    const handlePlayerHitWallMessage = (playerHitWallMessage: MessageType) => {
      setMessages([...messages, playerHitWallMessage]);
    };

    const handlePlayerWinsMessage = (playerWinsMessage: MessageType) => {
      setMessages([...messages, playerWinsMessage]);
    };

    socket.on("message", handleNewMessage);
    socket.on("giveUp", handleGiveUpMessage);
    socket.on("playerMoveLog", handlePlayerMoveMessage);
    socket.on("playerHitWallLog", handlePlayerHitWallMessage);
    socket.on("gameFinished", handlePlayerWinsMessage);
    return () => {
      socket.off("message");
      socket.off("giveUp");
      socket.off("playerMoveLog");
      socket.off("playerHitWallLog");
      socket.off("gameFinished");
    };
  }, [currentUser, messages, socket]);

  const sendMessage = () => {
    if (newMessage.trim() !== "") {
      if (newMessage.startsWith("/")) {
        const direction = newMessage.toLowerCase();
        socket.emit("command", direction);
      } else {
        socket.emit("message", { text: newMessage, sender: currentUser });
      }
      setNewMessage("");
    }
  };

  return (
    <div className="chat-container">
      <ul className="chat">
        {messages.map((message, index) => (
          <li
            key={index}
            className={getMessageClassName(message.type)}
            style={{
              textAlign: message.sender === currentUser ? "right" : "left",
            }}
          >
            {message.text}
          </li>
        ))}
      </ul>
      <div className="send-message-container">
        <input
          type="text"
          value={newMessage}
          className="chat-window"
          onChange={(e) => setNewMessage(e.target.value)}
        />
        <Button
          variant="contained"
          endIcon={<SendIcon />}
          onClick={sendMessage}
        >
          Send
        </Button>
      </div>
    </div>
  );
};
