import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from "@mui/material";
import { Maze } from "./maze";
import { ChangeEvent, useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router";
import { SocketContext } from "../context/socket";
import { Paths } from "../common/enums/paths.enum";
import { Chat } from "./chat";

export const Game = () => {
  const [playerName, setPlayerName] = useState("");
  const navigate = useNavigate();
  const socket = useContext(SocketContext);
  const [rows, setRows] = useState(20);
  const [cols, setCols] = useState(25);
  const [gameFinished, setGameFinished] = useState(false);
  const [confirmGiveUp, setConfirmGiveUp] = useState(false);
  const [winner, setWinner] = useState<boolean>(false);

  const handleRowsChange = (e: ChangeEvent<HTMLInputElement>) => {
    setRows(parseInt(e.target.value));
  };

  const handleColsChange = (e: ChangeEvent<HTMLInputElement>) => {
    setCols(parseInt(e.target.value));
  };

  const handleWinnerStatus = (winnerStatus: boolean) => {
    setWinner(winnerStatus);
  };

  const handleExit = () => {
    socket.emit("exitGame", playerName);
  };

  const handleGiveUp = () => {
    setConfirmGiveUp(true);
  };

  const handleConfirmGiveUp = () => {
    setConfirmGiveUp(false);
    setGameFinished(true);
    setWinner(true);
    socket.emit("giveUp", { playerName });
  };

  const handleCloseGiveUpDialog = () => {
    setConfirmGiveUp(false);
  };

  useEffect(() => {
    socket.on("setWinnerStatus", () => {
      setWinner(true);
    });

    socket.on("navigateToDashboard", () => {
      navigate(Paths.DASHBOARD);
    });

    return () => {
      socket.off("setWinnerStatus");
      socket.off("navigateToDashboard");
    };
  }, [navigate, socket]);

  useEffect(() => {
    const storedPlayerName = localStorage.getItem("userName");
    if (storedPlayerName) {
      setPlayerName(storedPlayerName);
      socket.emit("setPlayerName", storedPlayerName);
    }
  }, [playerName, socket]);

  return (
    <div className="game">
      {!gameFinished && (
        <Button
          color="secondary"
          variant="outlined"
          sx={{ marginBottom: "30px", marginRight: "50px", width: "250px" }}
          onClick={handleGiveUp}
        >
          Give up
        </Button>
      )}
      <Button
        variant="contained"
        color="error"
        sx={{ marginBottom: "30px", width: "200px" }}
        disabled={!winner}
        onClick={handleExit}
      >
        Exit
      </Button>
      <Chat currentUser={playerName} />
      <Dialog open={confirmGiveUp} onClose={handleCloseGiveUpDialog}>
        <DialogTitle>Confirm Give Up</DialogTitle>
        <DialogContent>Are you sure you want to give up?</DialogContent>
        <DialogActions>
          <Button onClick={handleConfirmGiveUp}>Yes</Button>
          <Button onClick={handleCloseGiveUpDialog}>No</Button>
        </DialogActions>
      </Dialog>
      <div className="rows-columns-container">
        <div>
          <label className="row-label">
            Rows:
            <input
              className="rows-input"
              type="number"
              value={rows}
              onChange={handleRowsChange}
            />
          </label>
        </div>
        <div>
          <label className="row-label">
            Columns:
            <input
              className="columns-input"
              type="number"
              value={cols}
              onChange={handleColsChange}
            />
          </label>
        </div>
      </div>
      <Maze rows={rows} cols={cols} handleWinnerStatus={handleWinnerStatus} />
    </div>
  );
};
