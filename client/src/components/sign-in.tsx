import { useState, useEffect, ChangeEvent } from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
} from "@mui/material";
import { Dashboard } from "./dashboard";

export const SignIn = () => {
  const [open, setOpen] = useState(false);
  const [name, setName] = useState("");

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setName(event.target.value);
  };

  const handleSignIn = () => {
    localStorage.setItem("userName", name);
    setOpen(false);
  };

  useEffect(() => {
    const storedName = localStorage.getItem("userName");
    if (storedName) {
      setName(storedName);
    } else {
      setOpen(true);
    }
  }, []);

  return (
    <div>
      {!name ? (
        <Button variant="contained" onClick={() => setOpen(true)}>
          Sign In
        </Button>
      ) : (
        <Dashboard name={name} />
      )}
      <Dialog open={open} onClose={() => setOpen(false)}>
        <DialogTitle>Sign In</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Enter your name"
            type="text"
            fullWidth
            placeholder="Jason Statham"
            value={name}
            onChange={handleChange}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setOpen(false)} color="primary">
            Cancel
          </Button>
          <Button onClick={handleSignIn} color="primary">
            Log In
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};
