import { createContext } from "react";
import type { Socket } from "socket.io-client";
import io from "socket.io-client";
import { SOCKET_URL } from "../common/constants/socket.constants";

const socket = io(SOCKET_URL);
const SocketContext = createContext<Socket>(socket);

export { socket, SocketContext };
