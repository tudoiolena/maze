import { Injectable } from '@nestjs/common';
import { CreateGameDto } from './dto/create-game.dto';
import { Game } from './entities/game.entity';
import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class GameService {
  private readonly games: Map<string, Game> = new Map<string, Game>();

  async create(createGameDto: CreateGameDto): Promise<Game> {
    const { playerName } = createGameDto;
    const game = new Game();
    game.id = uuidv4();
    game.playerName = playerName;
    game.initiatedAt = new Date();
    return game;
  }

  findAll() {
    return Array.from(this.games.values());
  }

  findOne(id: string) {
    return this.games.get(id);
  }
}
