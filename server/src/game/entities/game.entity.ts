import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
} from 'typeorm';

@Entity({ name: 'games' })
export class Game {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @CreateDateColumn({ type: 'timestamp with time zone', name: 'initiated_at' })
  initiatedAt: Date;

  @Column({
    type: 'varchar',
    nullable: true,
  })
  playerName: string;
}
