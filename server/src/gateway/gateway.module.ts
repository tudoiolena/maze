import { Module } from '@nestjs/common';
import { GameGateway } from './game.gateway';
import { ChatGateway } from './chat.gateway';
import { GameService } from 'src/game/game.service';
import { MazeService } from 'src/maze/maze.service';

@Module({
  providers: [GameGateway, ChatGateway, GameService, MazeService],
})
export class GatewayModule {}
