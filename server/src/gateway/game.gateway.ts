import {
  WebSocketGateway,
  WebSocketServer,
  SubscribeMessage,
  OnGatewayInit,
  OnGatewayConnection,
  OnGatewayDisconnect,
} from '@nestjs/websockets';

import { GameService } from '../game/game.service';
import { CreateGameDto } from 'src/game/dto/create-game.dto';
import { MazeService } from 'src/maze/maze.service';
@WebSocketGateway({ cors: '*:*' })
export class GameGateway
  implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect
{
  @WebSocketServer()
  server;

  constructor(
    private readonly gameService: GameService,
    private readonly mazeService: MazeService,
  ) {}
  private maze: number[][];
  private playerID: string = 'player1';
  private player1ClientID: string;
  private player2ClientID: string;
  private exitPosition: { row: number; col: number };
  private gameEnded: boolean = false;
  private players: {
    player1: { row: number; col: number };
    player2: { row: number; col: number };
  } = {
    player1: { row: 0, col: 0 },
    player2: { row: 0, col: 0 },
  };
  private readonly COMMANDS = {
    UP: '/up',
    DOWN: '/down',
    LEFT: '/left',
    RIGHT: '/right',
  };

  private readonly DIRECIONS = {
    UP: 'up',
    DOWN: 'down',
    LEFT: 'left',
    RIGHT: 'right',
  };

  afterInit() {}

  handleConnection() {}
  handleDisconnect() {}

  private emitPlayerCount() {
    const numberOfConnectedPlayers = Object.keys(this.players).length;
    this.server.emit('playerCount', numberOfConnectedPlayers);
  }

  @SubscribeMessage('createGame')
  async handleCreateGame(client: any, createGameDto: CreateGameDto) {
    const game = await this.gameService.create(createGameDto);
    this.player1ClientID = client.id;
    this.server.emit('gameCreated', game);
  }

  @SubscribeMessage('joinGame')
  handleJoinGame(client: any, data: { playerName: string }) {
    this.player2ClientID = client.id;
    this.server.emit('navigateToGame', { playerName: data.playerName });
  }

  @SubscribeMessage('setPlayerName')
  handleSetPlayerName(client: any, playerName: string) {
    if (this.players[client.id]) {
      this.players[client.id].name = playerName;
    }
  }

  @SubscribeMessage('exitGame')
  handleExitGame(client: any) {
    delete this.players[client.id];
    this.server.emit('gameFinished', { winner: true });
    this.server.emit('navigateToDashboard');
    this.emitPlayerCount();
  }

  @SubscribeMessage('giveUp')
  handleGiveUp(client: any, { playerName }) {
    const timestamp = new Date().toLocaleTimeString();
    const logMessage = {
      text: `${timestamp} Player ${playerName} gave up`,
      sender: 'System',
      type: 'giveUp',
      timestamp: Date.now(),
    };
    this.server.emit('giveUp', logMessage);
  }

  @SubscribeMessage('generateMaze')
  handleGenerateMaze(client: any, data: { rows: number; cols: number }) {
    const { rows, cols } = data;
    const { maze, exitPosition } = this.mazeService.generateMaze(rows, cols);
    const initialPlayer1Position =
      this.mazeService.generateInitialPlayerPosition(maze);
    const initialPlayer2Position =
      this.mazeService.generateInitialPlayerPosition(maze);
    const retrievedMaze = this.mazeService.getMaze();

    this.players.player1 = initialPlayer1Position;
    this.players.player2 = initialPlayer2Position;
    this.exitPosition = exitPosition;
    this.maze = retrievedMaze;

    client.emit('mazeState', {
      maze,
      exitPosition,
      initialPlayer1Position,
      initialPlayer2Position,
    });
    this.server.emit('mazeState', {
      maze,
      exitPosition,
      initialPlayer1Position,
      initialPlayer2Position,
    });
  }

  @SubscribeMessage('playerMoved')
  handlePlayerMoved(
    client: any,
    data: {
      player: string;
      newPosition: { row: number; col: number };
      direction: string;
    },
  ) {
    const { player, newPosition, direction } = data;

    if (player === 'player1' && this.player1ClientID === client.id) {
      this.players.player1.row = newPosition.row;
      this.players.player1.col = newPosition.col;
    } else if (player === 'player2' && this.player2ClientID === client.id) {
      this.players.player2.row = newPosition.row;
      this.players.player2.col = newPosition.col;
    }
    client.emit('playerMoved', data);
    this.server.emit('playerMoved', data);

    const timestamp = new Date().toLocaleTimeString();
    const logMessage = {
      text: `${timestamp} ${player} going ${direction}`,
      sender: 'System',
      type: 'playerMove',
      timestamp: Date.now(),
    };

    this.server.emit('playerMoveLog', logMessage);
  }

  @SubscribeMessage('playerHitWall')
  handlePlayerHitWall(
    client: any,
    data: { player: string; row: number; col: number },
  ) {
    this.server.emit('wallHit', {
      player: client.id,
      newPosition: data,
    });

    const { player } = data;
    const timestamp = new Date().toLocaleTimeString();
    const logMessage = {
      text: `${timestamp} ${player} hit the wall!`,
      sender: 'System',
      type: 'playerHitWall',
      timestamp: Date.now(),
    };

    this.server.emit('playerHitWallLog', logMessage);
  }

  @SubscribeMessage('command')
  handleChatCommand(client: any, direction: string) {
    if (
      (this.playerID === 'player1' && client.id === this.player1ClientID) ||
      (this.playerID === 'player2' && client.id === this.player2ClientID)
    ) {
      let newPosition = { row: 0, col: 0 };
      const currentPlayer = this.players[this.playerID];
      const movements = {
        [this.COMMANDS.UP]: { row: -1, col: 0 },
        [this.COMMANDS.DOWN]: { row: 1, col: 0 },
        [this.COMMANDS.LEFT]: { row: 0, col: -1 },
        [this.COMMANDS.RIGHT]: { row: 0, col: 1 },
      };

      if (direction in movements) {
        const movement = movements[direction];
        newPosition = {
          row: currentPlayer.row + movement.row,
          col: currentPlayer.col + movement.col,
        };
      }

      if (this.maze[newPosition.row][newPosition.col] === 1) {
        this.handlePlayerMoved(client, {
          player: this.playerID,
          newPosition,
          direction,
        });

        if (
          newPosition.row === this.exitPosition.row &&
          newPosition.col === this.exitPosition.col
        ) {
          const winner = this.playerID === 'player1' ? 'Player 1' : 'Player 2';
          this.handlePlayerWins(client, winner);
          this.gameEnded = true;
        }
        this.handleTurn(client, this.playerID);
      } else {
        this.handlePlayerHitWall(client, {
          player: this.playerID,
          row: newPosition.row,
          col: newPosition.col,
        });
      }
    }
  }

  @SubscribeMessage('whoseTurn')
  handleTurn(client: any, playerID: string) {
    this.playerID = this.playerID === 'player1' ? 'player2' : 'player1';

    const messageYourTurn = "It's Player 1 turn";
    const messageOpponentTurn = "It's Player 2 turn";

    const clientMessage =
      playerID === 'player1' ? messageYourTurn : messageOpponentTurn;

    const oppositeClientMessage =
      playerID === 'player2' ? messageOpponentTurn : messageYourTurn;

    const opponentClient =
      playerID === 'player1' ? this.player2ClientID : this.player1ClientID;
    client.emit('turnMessage', clientMessage);
    this.server.to(opponentClient).emit('turnMessage', oppositeClientMessage);
  }

  @SubscribeMessage('gameFinished')
  handlePlayerWins(client: any, winner: string) {
    const timestamp = new Date().toLocaleTimeString();
    const logMessage = {
      text: `${timestamp} ${winner}, you win!!!`,
      sender: 'System',
      type: 'playerWins',
      timestamp: Date.now(),
    };
    this.server.emit('gameFinished', logMessage);
    this.server.emit('setWinnerStatus', true);
  }

  @SubscribeMessage('move')
  handleMove(client: any, direction: string) {
    if (this.gameEnded) {
      return;
    }

    if (
      (this.playerID === 'player1' && client.id === this.player1ClientID) ||
      (this.playerID === 'player2' && client.id === this.player2ClientID)
    ) {
      let newPosition = { row: 0, col: 0 };
      const currentPlayer = this.players[this.playerID];
      const movements = {
        [this.DIRECIONS.UP]: { row: -1, col: 0 },
        [this.DIRECIONS.DOWN]: { row: 1, col: 0 },
        [this.DIRECIONS.LEFT]: { row: 0, col: -1 },
        [this.DIRECIONS.RIGHT]: { row: 0, col: 1 },
      };

      if (direction in movements) {
        const movement = movements[direction];
        newPosition = {
          row: currentPlayer.row + movement.row,
          col: currentPlayer.col + movement.col,
        };
      }

      if (this.maze[newPosition.row][newPosition.col] === 1) {
        this.handlePlayerMoved(client, {
          player: this.playerID,
          newPosition,
          direction,
        });

        if (
          newPosition.row === this.exitPosition.row &&
          newPosition.col === this.exitPosition.col
        ) {
          const winner = this.playerID === 'player1' ? 'Player 1' : 'Player 2';
          this.handlePlayerWins(client, winner);
          this.gameEnded = true;
        }
        this.handleTurn(client, this.playerID);
      } else {
        this.handlePlayerHitWall(client, {
          player: this.playerID,
          row: newPosition.row,
          col: newPosition.col,
        });
      }
    }
  }
}
