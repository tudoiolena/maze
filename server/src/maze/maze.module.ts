import { Module } from '@nestjs/common';
import { MazeService } from './maze.service';

@Module({
  providers: [MazeService],
})
export class MazeModule {}
