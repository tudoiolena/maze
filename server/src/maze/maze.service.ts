import { Injectable } from '@nestjs/common';

@Injectable()
export class MazeService {
  private maze: number[][] = [];

  generateEmptyMaze(rows: number, cols: number): number[][] {
    const newMaze: number[][] = [];
    for (let i = 0; i < rows; i++) {
      newMaze.push(Array(cols).fill(0));
    }
    return newMaze;
  }

  recursiveBacktracking(maze: number[][], row: number, col: number): void {
    const directions = [
      [0, 1],
      [1, 0],
      [0, -1],
      [-1, 0],
    ];
    directions.sort(() => Math.random() - 0.5);

    for (const direction of directions) {
      const [dx, dy] = direction;
      const newRow = row + dx * 2;
      const newCol = col + dy * 2;

      if (
        newRow >= 0 &&
        newRow < maze.length &&
        newCol >= 0 &&
        newCol < maze[0].length &&
        maze[newRow][newCol] === 0
      ) {
        maze[newRow][newCol] = 1;
        maze[row + dx][col + dy] = 1;
        this.recursiveBacktracking(maze, newRow, newCol);
      }
    }
  }

  generateMaze(
    rows: number,
    cols: number,
  ): { maze: number[][]; exitPosition: { row: number; col: number } } {
    this.maze = this.generateEmptyMaze(rows, cols);
    this.recursiveBacktracking(this.maze, 0, 0);

    let exitPosition = this.generateExitPosition(this.maze);
    while (this.maze[exitPosition.row][exitPosition.col] !== 1) {
      exitPosition = this.generateExitPosition(this.maze);
    }

    return { maze: this.maze, exitPosition };
  }

  generateExitPosition(maze: number[][]): { row: number; col: number } {
    return {
      row: Math.floor(Math.random() * maze.length),
      col: Math.floor(Math.random() * maze[0].length),
    };
  }

  generateInitialPlayerPosition(maze: number[][]): {
    row: number;
    col: number;
  } {
    let initialPosition = this.generateExitPosition(maze);
    while (maze[initialPosition.row][initialPosition.col] !== 1) {
      initialPosition = this.generateExitPosition(maze);
    }
    return initialPosition;
  }

  getMaze(): number[][] {
    if (this.maze.length === 0) {
      throw new Error('Maze has not been generated yet.');
    }
    return this.maze;
  }
}
