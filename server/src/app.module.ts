import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { GameModule } from './game/game.module';
import { GatewayModule } from './gateway/gateway.module';
import { MazeModule } from './maze/maze.module';

@Module({
  imports: [GatewayModule, GameModule, MazeModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
